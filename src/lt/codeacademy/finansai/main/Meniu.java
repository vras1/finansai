package lt.codeacademy.finansai.main;

public class Meniu {

	Zinutes zinutes = new Zinutes();
	Spausdink spausdink = new Spausdink();
	Ivesk ivesk = new Ivesk();
	Pajamos pajamos = new Pajamos();
	PajamuIrasas pajamuI = new PajamuIrasas();
	Islaidos islaidos = new Islaidos();
	IslaiduIrasas islaiduI = new IslaiduIrasas();
	Balansas balansas = new Balansas();

	String[] pajamuKategorija = new String[9];
	String[] pajamuDuomenuKategorija = new String[10];
	String[] islaiduKategorija = new String[9];
	String[] islaiduDuomenuKategorija = new String[10];
	String[] balansoKategorija = new String[4];

	void pagrindinisMeniu() {

		String[] meniu;
		meniu = new String[4];
		meniu[0] = " -> Peržiūrėti įvestus duomenis (1)";
		meniu[1] = " -> Įvesti pajamų informaciją (2)";
		meniu[2] = " -> Įvesti išlaidų informaciją (3)";
		meniu[3] = " -> Išeiti (4)";
		int pasirinkimas = 0;
		while (true) {
			spausdink.spausdinkMenu(meniu);
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");
			spausdink.spausdink("Jūs pasirinkote" + meniu[pasirinkimas]);

			switch (pasirinkimas) {
			case 0:
				duomenuPerziura();
				break;
			case 1:
				pajamuKategorija();
				break;
			case 2:
				islaiduKategorija();
				break;
			case 3:
				spausdink.spausdink("");
				spausdink.spausdink("-> Baigta <-");
				System.exit(0);
				break;
			default:
				zinutes.blogas();
				break;
			}
		}
	}

	void pajamuMeniu() {

		pajamuKategorija[0] = " -> Darbo užmokestis (1)";
		pajamuKategorija[1] = " -> NT nuoma (2)";
		pajamuKategorija[2] = " -> Individuali veikla (3)";
		pajamuKategorija[3] = " -> Dividentai (4)";
		pajamuKategorija[4] = " -> Stipendija (5)";
		pajamuKategorija[5] = " -> Soc. išmokos (6)";
		pajamuKategorija[6] = " -> Dovana (7)";
		pajamuKategorija[7] = " -> Kitos pajamos (8)";
		pajamuKategorija[8] = " -> Grįžti (9)";
		spausdink.spausdinkMenu(pajamuKategorija);
	}

	void pajamuDuomenuMeniu() {

		pajamuDuomenuKategorija[0] = " -> Darbo užmokestis (1)";
		pajamuDuomenuKategorija[1] = " -> NT nuoma (2)";
		pajamuDuomenuKategorija[2] = " -> Individuali veikla (3)";
		pajamuDuomenuKategorija[3] = " -> Dividentai (4)";
		pajamuDuomenuKategorija[4] = " -> Stipendija (5)";
		pajamuDuomenuKategorija[5] = " -> Soc. išmokos (6)";
		pajamuDuomenuKategorija[6] = " -> Dovana (7)";
		pajamuDuomenuKategorija[7] = " -> Kitos pajamos (8)";
		pajamuDuomenuKategorija[8] = " -> Visa informacija (9)";
		pajamuDuomenuKategorija[9] = " -> Grįžti (10)";
		spausdink.spausdinkMenu(pajamuDuomenuKategorija);
	}

	void pajamuDuomenys() {

		spausdink.spausdink("Pasirinkite duomenis");
		spausdink.spausdink("");
		int pasirinkimas = 0;

		while (true) {

			pajamuDuomenuMeniu();
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");

			switch (pasirinkimas) {
			case 0:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;
			case 1:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 2:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 3:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 4:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 5:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 6:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 7:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 8:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 9:
				spausdink.spausdink(pajamuI.toString(pasirinkimas));
				spausdink.spausdink("");
				return;

			default:
				zinutes.blogas();
				break;
			}

		}

	}

	void islaiduMeniu() {

		islaiduKategorija[0] = " -> Maistui (1)";
		islaiduKategorija[1] = " -> Aprangai (2)";
		islaiduKategorija[2] = " -> Automobiliui (3)";
		islaiduKategorija[3] = " -> Sveikatai (4)";
		islaiduKategorija[4] = " -> Mokslams (5)";
		islaiduKategorija[5] = " -> Pramogoms (6)";
		islaiduKategorija[6] = " -> Restoranams (7)";
		islaiduKategorija[7] = " -> Mokesčiams ir įmokoms (8)";
		islaiduKategorija[8] = " -> Grįžti (9)";
		spausdink.spausdinkMenu(islaiduKategorija);
	}

	void islaiduDuomenuMeniu() {

		islaiduDuomenuKategorija[0] = " -> Maistui (1)";
		islaiduDuomenuKategorija[1] = " -> Aprangai (2)";
		islaiduDuomenuKategorija[2] = " -> Automobiliui (3)";
		islaiduDuomenuKategorija[3] = " -> Sveikatai (4)";
		islaiduDuomenuKategorija[4] = " -> Mokslams (5)";
		islaiduDuomenuKategorija[5] = " -> Pramogoms (6)";
		islaiduDuomenuKategorija[6] = " -> Restoranams (7)";
		islaiduDuomenuKategorija[7] = " -> Mokesčiams ir įmokoms (8)";
		islaiduDuomenuKategorija[8] = " -> Visa informacija (9)";
		islaiduDuomenuKategorija[9] = " -> Grįžti (10)";
		spausdink.spausdinkMenu(islaiduDuomenuKategorija);
	}

	void islaiduDuomenys() {

		spausdink.spausdink("Pasirinkite duomenis");
		spausdink.spausdink("");
		int pasirinkimas = 0;

		while (true) {

			islaiduDuomenuMeniu();
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");

			switch (pasirinkimas) {
			case 0:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;
			case 1:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 2:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 3:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 4:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 5:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 6:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 7:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 8:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				break;

			case 9:
				spausdink.spausdink(islaiduI.toString(pasirinkimas));
				spausdink.spausdink("");
				return;

			default:
				zinutes.blogas();
				break;
			}

		}

	}

	void duomenuPerziura() {

		spausdink.spausdink("Duomenų peržiūra");
		spausdink.spausdink("");
		String[] meniu;
		meniu = new String[4];
		meniu[0] = " -> Pajamų duomenys (1)";
		meniu[1] = " -> Išlaidų duomenys (2)";
		meniu[2] = " -> Balanso duomenys (3)";
		meniu[3] = " -> Grįžti (4)";
		int pasirinkimas = 0;

		while (true) {

			spausdink.spausdinkMenu(meniu);
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");
			spausdink.spausdink("Jūs pasirinkote" + meniu[pasirinkimas]);

			switch (pasirinkimas) {
			case 0:
				pajamuDuomenys();
				break;
			case 1:
				islaiduDuomenys();
				break;
			case 2:
				balansas();
				break;
			case 3:
				return;
			default:
				zinutes.blogas();
				break;
			}
		}
	}

	void pajamuInfoIvedimas() {

		pajamos.setId();
		pajamos.setSuma(ivesk.iveskSuma());
		pajamos.setData(ivesk.data());
		pajamos.setGrynais(ivesk.arGrynais());
		pajamos.setPavedimu(ivesk.arPavedimu());

	}

	void islaiduInfoIvedimas() {

		islaidos.setId();
		islaidos.setSuma(ivesk.iveskSuma());
		islaidos.setData(ivesk.data2());
		islaidos.setGrynais(ivesk.arGrynais());
		islaidos.setPavedimu(ivesk.arPavedimu());

	}

	void pajamuKategorija() {

		spausdink.spausdink("Pasirinkite pajamų kilmę");
		spausdink.spausdink("");
		int pasirinkimas = 0;

		while (true) {

			pajamuMeniu();
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");

			switch (pasirinkimas) {
			case 0:
				pajamos.setKategorija("Darbo užmokestis");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 1:
				pajamuInfoIvedimas();
				pajamos.setKategorija("NT nuoma");
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 2:
				pajamos.setKategorija("Individuali veikla");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 3:
				pajamos.setKategorija("Dividentai");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 4:
				pajamos.setKategorija("Stipendija");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 5:
				pajamos.setKategorija("Soc. išmokos");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 6:
				pajamos.setKategorija("Dovana");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 7:
				pajamos.setKategorija("Kitos pajamos");
				pajamuInfoIvedimas();
				pajamuI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 8:
				return;
			default:
				zinutes.blogas();
				break;
			}

		}
	}

	void islaiduKategorija() {

		spausdink.spausdink("Pasirinkite išlaidas");
		spausdink.spausdink("");
		int pasirinkimas = 0;

		while (true) {

			islaiduMeniu();
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");

			switch (pasirinkimas) {
			case 0:
				islaidos.setKategorija("Maistui");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 1:
				islaiduInfoIvedimas();
				islaidos.setKategorija("Aprangai");
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 2:
				islaidos.setKategorija("Automobiliui");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 3:
				islaidos.setKategorija("Sveikatai");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 4:
				islaidos.setKategorija("Mokslams");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 5:
				islaidos.setKategorija("Pramogoms");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 6:
				islaidos.setKategorija("Restoranams");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 7:
				islaidos.setKategorija("Mokesčiams ir įmokoms");
				islaiduInfoIvedimas();
				islaiduI.issaugok(pasirinkimas);
				spausdink.spausdink("");
				break;
			case 8:
				return;
			default:
				zinutes.blogas();
				break;
			}

		}
	}

	void balansoMeniu() {

		balansoKategorija[0] = " -> Pajamos (1)";
		balansoKategorija[1] = " -> Išlaidos (2)";
		balansoKategorija[2] = " -> Balansas (3)";
		balansoKategorija[3] = " -> Grįžti (4)";
		spausdink.spausdinkMenu(balansoKategorija);
	}

	void balansas() {

		spausdink.spausdink("Pasirinkite balanso duomenis");
		spausdink.spausdink("");
		int pasirinkimas = 0;

		while (true) {

			balansoMeniu();
			pasirinkimas = ivesk.ivesk();
			spausdink.spausdink("");

			switch (pasirinkimas) {
			case 0:
				balansas.pajamos();
				spausdink.spausdink("Visos pajamos: " + balansas.pa + " EUR");
				spausdink.spausdink("");
				break;
			case 1:
				balansas.islaidos();
				spausdink.spausdink("Visos išlaidos: " + balansas.is + " EUR");
				spausdink.spausdink("");
				break;

			case 2:
				balansas.balansas();
				spausdink.spausdink("");
				break;

			case 3:
				return;

			default:
				zinutes.blogas();
				break;
			}

		}

	}

}
