package lt.codeacademy.finansai.main;

public class Balansas {

	int[] pajamos = new int[8];
	int[] islaidos = new int[8];
	int pa = 0;
	int is = 0;

	public void pajamos() {

		pajamos[0] = PajamuIrasas.getDuomenys();
		pajamos[1] = PajamuIrasas.getDuomenys2();
		pajamos[2] = PajamuIrasas.getDuomenys3();
		pajamos[3] = PajamuIrasas.getDuomenys4();
		pajamos[4] = PajamuIrasas.getDuomenys5();
		pajamos[5] = PajamuIrasas.getDuomenys6();
		pajamos[6] = PajamuIrasas.getDuomenys7();
		pajamos[7] = PajamuIrasas.getDuomenys8();

		for (int i = 0; i < pajamos.length; i++) {

			if (pajamos[i] != 0) {
				pa = pa + pajamos[i];
			}
		}

	}

	void islaidos() {

		islaidos[0] = IslaiduIrasas.getDuomenys();
		islaidos[1] = IslaiduIrasas.getDuomenys2();
		islaidos[2] = IslaiduIrasas.getDuomenys3();
		islaidos[3] = IslaiduIrasas.getDuomenys4();
		islaidos[4] = IslaiduIrasas.getDuomenys5();
		islaidos[5] = IslaiduIrasas.getDuomenys6();
		islaidos[6] = IslaiduIrasas.getDuomenys7();
		islaidos[7] = IslaiduIrasas.getDuomenys8();

		for (int i = 0; i < islaidos.length; i++) {

			if (islaidos[i] != 0) {
				is = is + islaidos[i];
			}
		}

	}

	void balansas() {
		pajamos();
		islaidos();
		int balansas = 0;
		balansas = pa - is;
		System.out.println("Jūsų balansas: " + balansas + " EUR");

	}

}
