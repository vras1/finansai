package lt.codeacademy.finansai.main;

import java.time.format.DateTimeFormatter;
import java.util.Arrays;

public class PajamuIrasas {

	int counter = 0;
	static String[] duomenys, duomenys2, duomenys3, duomenys4, duomenys5, duomenys6, duomenys7, duomenys8;

	public static int getDuomenys() {
		if (duomenys != null) {
			return Integer.parseInt(duomenys[2].substring(6, duomenys[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys2() {
		if (duomenys2 != null) {
			return Integer.parseInt(duomenys2[2].substring(6, duomenys2[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys3() {
		if (duomenys3 != null) {
			return Integer.parseInt(duomenys3[2].substring(6, duomenys3[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys4() {
		if (duomenys4 != null) {

			return Integer.parseInt(duomenys4[2].substring(6, duomenys4[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys5() {
		if (duomenys5 != null) {

			return Integer.parseInt(duomenys5[2].substring(6, duomenys5[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys6() {
		if (duomenys6 != null) {

			return Integer.parseInt(duomenys6[2].substring(6, duomenys6[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys7() {
		if (duomenys7 != null) {

			return Integer.parseInt(duomenys7[2].substring(6, duomenys7[2].length() - 4));
		}
		return 0;
	}

	public static int getDuomenys8() {
		if (duomenys8 != null) {

			return Integer.parseInt(duomenys8[2].substring(6, duomenys8[2].length() - 4));
		}
		return 0;
	}

	public void issaugok(int i) {

		switch (i) {
		case 0:
			duomenys = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 1:
			duomenys2 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 2:
			duomenys3 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 3:
			duomenys4 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 4:
			duomenys5 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 5:
			duomenys6 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 6:
			duomenys7 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;
		case 7:
			duomenys8 = new String[] { "ID: " + Integer.toString(Pajamos.getId()),
					"kategorija: " + Pajamos.getKategorija(), "suma: " + Integer.toString(Pajamos.getSuma()) + " EUR",
					"data: " + Pajamos.getData().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")),
					"grynais: " + Boolean.toString(Pajamos.isGrynais()),
					"pavedimu: " + Boolean.toString(Pajamos.isPavedimu()) };

			break;

		default:
			break;
		}
	}

	public String toString(int i) {
		switch (i) {
		case 0:
			if (duomenys == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys);
			}
		case 1:
			if (duomenys2 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys2);
			}
		case 2:
			if (duomenys3 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys3);
			}
		case 3:
			if (duomenys4 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys4);
			}
		case 4:
			if (duomenys5 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys5);
			}
		case 5:
			if (duomenys6 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys6);
			}
		case 6:
			if (duomenys7 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys7);
			}
		case 7:
			if (duomenys8 == null) {
				return "Duomenų nėra!";
			} else {
				return Arrays.toString(duomenys8);
			}
		case 8:

			if (duomenys != null || duomenys2 != null || duomenys3 != null || duomenys4 != null || duomenys5 != null
					|| duomenys6 != null || duomenys7 != null || duomenys8 != null) {
				return (Arrays.toString(duomenys) + "\n" + Arrays.toString(duomenys2) + "\n"
						+ Arrays.toString(duomenys3) + "\n" + Arrays.toString(duomenys4) + "\n"
						+ Arrays.toString(duomenys5) + "\n" + Arrays.toString(duomenys6) + "\n"
						+ Arrays.toString(duomenys7) + "\n" + Arrays.toString(duomenys8));

			} else {
				return "Duomenų nėra!";
			}
		default:
			break;
		}
		return "";

	}

}
