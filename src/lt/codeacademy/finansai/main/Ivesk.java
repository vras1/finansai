package lt.codeacademy.finansai.main;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;

public class Ivesk {

	private Scanner sc;

	// Pinigu suma
	int iveskSuma() {

		System.out.println("");
		sc = new Scanner(System.in);
		System.out.println("Įveskite sumą: ");
		int sk = sc.nextInt();
		return sk;

	}

	// Pasirinkimas
	int ivesk() {

		System.out.println("");
		sc = new Scanner(System.in);
		System.out.println("Jusu pasirinkimas: ");
		int sk = sc.nextInt();
		return sk - 1;

	}

	// Data
	public LocalDate data() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate data = null;
		try {
			data = LocalDate.parse(iveskData(), format);
		} catch (Klaida e) {
			System.out.println("Bloga data");
		} finally {
			System.out.println("Blogai įvesta data!");
		}
		return data;
	}

	public LocalDate data2() {
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDate data = null;
		try {
			data = LocalDate.parse(iveskData2(), format);
		} catch (Klaida e) {
			System.out.println("Bloga data");
		} finally {
			System.out.println("Blogai įvesta data!");
		}
		return data;
	}

	CharSequence iveskData() throws Klaida {
		sc = new Scanner(System.in);
		System.out.println("");
		System.out.println("Kada gauti pinigai?" + "\n" + "Įveskite datą formatu yyyy-mm-dd ");
		String sk = sc.nextLine();
		return sk;

	}

	CharSequence iveskData2() throws Klaida{
		sc = new Scanner(System.in);
		System.out.println("");
		System.out.println("Kada išleisti pinigai?" + "\n" + "Įveskite datą formatu yyyy-mm-dd ");
		String sk = sc.nextLine();
		return sk;

	}

	// ar grynais
	public boolean arGrynais() {

		System.out.println("");
		sc = new Scanner(System.in);
		System.out.println("Grynais?" + "\n" + "Atsakykite: taip/ne");
		String zodis = sc.nextLine();
		if ("taip".equals(zodis)) {
			return true;
		}
		return false;
	}

	// ar pavedimu
	public boolean arPavedimu() {

		System.out.println("");
		sc = new Scanner(System.in);
		System.out.println("Pavedimu?" + "\n" + "Atsakykite: taip/ne");
		String zodis = sc.nextLine();
		if ("taip".equals(zodis)) {
			return true;
		}
		return false;
	}

}
