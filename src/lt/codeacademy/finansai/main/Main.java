package lt.codeacademy.finansai.main;

public class Main {

	public static void main(String[] args) {

		Meniu meniu = new Meniu();
		Zinutes zinutes = new Zinutes();
		Ivesk ivesk = new Ivesk();
		Spausdink spausdink = new Spausdink();

		zinutes.welcome();
		meniu.pagrindinisMeniu();
		ivesk.ivesk();
		spausdink.spausdinkMenu(args);

	}

}
