package lt.codeacademy.finansai.main;

import java.time.LocalDate;

public class Islaidos {
	private static int id;
	private static int suma;
	private static LocalDate data;
	private static boolean grynais;
	private static boolean pavedimu;
	private static String kategorija;

	public void setId() {
		Islaidos.id++;
	}

	public void setSuma(int suma) {
		Islaidos.suma = suma;
	}

	public void setData(LocalDate data) {
		Islaidos.data = data;
	}

	public void setGrynais(boolean grynais) {
		Islaidos.grynais = grynais;
	}

	public void setPavedimu(boolean pavedimu) {
		Islaidos.pavedimu = pavedimu;
	}

	public void setKategorija(String kategorija) {
		Islaidos.kategorija = kategorija;
	}

	public static int getId() {
		return id;
	}

	public void setId(int id) {
		Islaidos.id = id;
	}

	public static int getSuma() {
		return suma;
	}

	public static LocalDate getData() {
		return data;
	}

	public static boolean isGrynais() {
		return grynais;
	}

	public static boolean isPavedimu() {
		return pavedimu;
	}

	public static String getKategorija() {
		return kategorija;
	}
}
