package lt.codeacademy.finansai.main;

import java.time.LocalDate;

public class Pajamos {

	private static int id;
	private static int suma;
	private static LocalDate data;
	private static boolean grynais;
	private static boolean pavedimu;
	private static String kategorija;

	public void setId() {
		Pajamos.id++;
	}

	public void setSuma(int suma) {
		Pajamos.suma = suma;
	}

	public void setData(LocalDate data) {
		Pajamos.data = data;
	}

	public void setGrynais(boolean grynais) {
		Pajamos.grynais = grynais;
	}

	public void setPavedimu(boolean pavedimu) {
		Pajamos.pavedimu = pavedimu;
	}

	public void setKategorija(String kategorija) {
		Pajamos.kategorija = kategorija;
	}

	public static int getId() {
		return id;
	}

	public void setId(int id) {
		Pajamos.id = id;
	}

	public static int getSuma() {
		return suma;
	}

	public static LocalDate getData() {
		return data;
	}

	public static boolean isGrynais() {
		return grynais;
	}

	public static boolean isPavedimu() {
		return pavedimu;
	}

	public static String getKategorija() {
		return kategorija;
	}

}
